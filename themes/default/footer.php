		<div class="main-wrap">
		
				<footer class="footer">
<div class="content">
	<div class="container">
  		
      <div class="row"> <!-- /.col -->

<div class="col-sm-3">

          <div class="heading-block">
            <h4>SwitchMarket</h4>
          </div> <!-- /.heading-block -->

          <ul class="icons-list">

 <li>
              <i class="icon-li fa fa-question-circle"></i>
               <a  href="./page-about.html">
              About Us
            </a>
            </li>
            
            <li>
              <i class="icon-li fa fa-support"></i>
               <a href="http://kb.switchmarket.io/help/article/link/switchmarket-faq">
              Knowledgebase
            </a>
            </li>

            <li>
              <i class="icon-li fa fa-envelope"></i>
              <a href="./page-contact.html">
              Contact
            </a>
            </li>
<!--
            <li>
              <i class="icon-li fa fa-unlock"></i>
               <a  href="./account-login.html">
                Login
                </a>
            </li>
      -->      
            
          </ul>
          
        </div>
        
        <div class="col-sm-3">

          <div class="heading-block">
            <h4>Connect With Us</h4>
          </div> <!-- /.heading-block -->

          <ul class="icons-list">

 <li>
              <i class="icon-li fa fa-envelope"></i>
              <a href="mailto:info@switchmarket.io">info@switchmarket.io</a>
            </li>
            
            <li>
              <i class="icon-li fa fa-phone"></i>
               UK: +44 330 043 0862<br />
			   SA: +27 21 201 6688
            </li>

          </ul>
          
        </div> 
        
        <div class="col-sm-3">

          <div class="heading-block">
            <h4>Find Us</h4>
          </div> <!-- /.heading-block -->

          <ul class="icons-list">

 <li>
              <i class="icon-li fa fa-map-pin"></i>
              16 Blackfriars Street<br>
Salford<br>
England<br>
M3 5BQ
            </li>
            
          </ul>
          
        </div>
        <!-- /.col --><!-- /.col -->

      </div> 
      <!-- /.row -->

	</div> <!-- /.container -->
</div>
</footer>

<footer class="copyright">
  <div class="container">

    <div class="row">

      <div class="col-sm-12">
        <span>Copyright &copy; 2016 SwitchMarket.</span>
      </div> <!-- /.col -->

    </div> <!-- /.row -->
    
  </div>
</footer>

	        </div>
		
		<!-- Core JS -->
<script src="<?php echo theme_url('/js/jquery.js'); ?>"></script>
<script src="<?php echo theme_url('/js/bootstrap.min.js'); ?>"></script>


<!-- App JS -->
<script src=".<?php echo theme_url('/js/mvpready-core.js'); ?>"></script>
<script src="<?php echo theme_url('/js/mvpready-helpers.js'); ?>"></script>
<script src="<?php echo theme_url('/js/mvpready-landing.js'); ?>"></script>
    </body>
</html>
