<?php theme_include('header'); ?>

	<section class="content wrap">
	
	<div class=" content-block">

      <div class="error-container">

        <div class="error-code">
        404
        </div> <!-- /.error-code -->

        <div class="error-details">

          <h4>Oops, <span class="text-primary">You're lost</span>.</h4>

          Unfortunately, the page <code>/<?php echo htmlspecialchars(current_url()); ?></code> could not be found. Try going back a step or contact us at <a href="mailto:info@switchmarket.io">info@switchmarket.io</a></p>

        </div> <!-- /.error-details -->

      </div> <!-- /.error -->

    </div> <!-- /.container -->
	
	</section>

<?php theme_include('footer'); ?>
