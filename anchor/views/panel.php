<?php echo $header; ?>

<hgroup class="wrap">
	<h1><?php echo __('panel.panel', 'Administration Panel'); ?></h1>
</hgroup>

<section class="wrap">
	<h3><?php echo __('Welcome to the admin panel', 'Welcome'); ?></h3>
	</br>
	<p><?php echo __('Here you will find all of the tools you will need to produce content for your website, manage users, posts and pages.'); ?></p>
</section>
<br />
<br />
<br />
<?php echo $footer; ?>