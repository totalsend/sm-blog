<nav>
	<a class="icon bold fa fa-bold" href="#bold"></a>&nbsp;
	<a class="icon italic fa fa-italic" href="#italic"></a>&nbsp;
	<a class="icon list fa fa-list" href="#list"></a>&nbsp;
	<a class="icon quote fa fa-quote-right" href="#quote"></a>&nbsp;
	<a class="icon code fa fa-code" href="#code"></a>&nbsp;
	<a class="icon link fa fa-link" href="#link"></a>
</nav>